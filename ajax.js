(function ($) {
    
      $('#btnLoadText').click(function () { $("#showResult").load("show.txt"); });
      $('#btnAjax').click(function () { callRestAPI() });
    
      // Perform an asynchronous HTTP (Ajax) API request.
      function callRestAPI() {
        var root = 'http://localhost:3000/api';
        $.ajax({
          url: root + '/movies/1',
          method: 'GET'
        }).then(function (response) {
          console.log(response);
          $('#showResult').html("<h3>Movie : "+response.movie+"<br>Year: "+response.year+"</h3");
        });
      }
    })($);